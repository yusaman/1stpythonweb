#how to run

# pull from Docker hub
1. docker run -p 4000:80 yusaman/ytestpythonweb:dev
2. browse  http://localhost:4000 or http://your_docker_ip:4000


# Git ( Bitbucket )
1. git clone https://yusaman@bitbucket.org/yusaman/1stpythonweb.git
2. cd 1stpythonweb
3. docker build -t yuthpythonweb .
4. docker run -p 4000:80 yuthpythonweb
5. browse  http://localhost:4000 or http://your_docker_ip:4000