var map;
var input = document.getElementById('search-input');

function initMap()
{
	//map
	map = new google.maps.Map(document.getElementById('map'), {
	  center: {
	    lat: -33.8688,
	    lng: 151.2195
	  },
	  zoom: 13,
	  draggable: false,
	  scrollwheel: false,
	  mapTypeId: 'roadmap'
	});

	//search
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	//stay displaying search box
	map.addListener('bounds_changed', function() {
	  console.log( 'map bound is changed' );
	  searchBox.setBounds(map.getBounds());
	});

	var markers = [];

	//searchbox listener
	searchBox.addListener('places_changed', function() {
		console.log( '1map move to new plage.');
	  	console.log( '1place: ', places );
	  var places = searchBox.getPlaces();

	  if (places.length == 0) {
	    return;
	  }
	  console.log( 'map move to new plage.');
	  console.log( 'place: ', places );
	  console.log( 'place json : ', places[0].geometry.location.toJSON() );
	  console.log( 'lat : ', places[0].geometry.location.lat() );
	  console.log( 'lng : ', places[0].geometry.location.lng() )
	  
	  //remove markers
	  markers.forEach(function(marker) {
	    marker.setMap(null);
	  });
	  markers = [];

	  var bounds = new google.maps.LatLngBounds();
	  places.forEach(function(place) {
	    if (!place.geometry) {
	      console.log("Returned place contains no geometry");
	      return;
	    }
	    var icon = {
	      url: place.icon,
	      size: new google.maps.Size(71, 71),
	      origin: new google.maps.Point(0, 0),
	      anchor: new google.maps.Point(17, 34),
	      scaledSize: new google.maps.Size(25, 25)
	    };

	    console.log( 'place2 : ', place.geometry.location );
	    console.log( 'lat : ', place.geometry.location.lat() );
	    console.log( 'lng : ', place.geometry.location.lng() )
		jQuery.ajax({
			method: "GET",
			url: "./search?q="+place.name+"&radius=70&lat="+place.geometry.location.lng()+"&lng="+place.geometry.location.lng(),
			success: function(responses){
				console.log( 'responses : ', responses );
				if( responses.length > 0 )
				{
					responses.results.forEach(function(response) {
						var data = {
							name: place.name,
							position: place.geometry.location,
							contentString: response.results.text
						}

						createInfoWindow(map, data);
					});
				}

			},
			error: function(error){
				console.log( 'error: ', error );
			}
		});


	    markers.push(new google.maps.Marker({
	      map: map,
	      icon: icon,
	      title: place.name,
	      position: place.geometry.location
	    }));

	    if (place.geometry.viewport) {
	      bounds.union(place.geometry.viewport);
	    } else {
	      bounds.extend(place.geometry.location);
	    }

	  });
	  map.fitBounds(bounds);
	});
}

function createInfoWindow(map, data) {
	var infowindow = new google.maps.InfoWindow({
      content: data.contentString
    });

    var marker = new google.maps.Marker({
      map: map,
      title: data.name,
      position: data.position
    });

	marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
}

function getUserLocation()
{
if( navigator.geolocation != undefined )
  {
    var geonav = navigator.geolocation.getCurrentPosition(function(position){
    user_pos = {
      lat:position.coords.latitude,
      lng:position.coords.longitude
    };
    var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    map.setCenter(pos);
    map.setZoom(10);
    // var your_marker = new google.maps.Marker({
    //   position: pos,
    //   map: map,
    //   animation: google.maps.Animation.DROP,
    //   title: 'you_are_here'
    // });

    });
  } 
}

$("#search-button").on('click', function(ev){
$("#search-input").trigger('change');
});


initMap();
getUserLocation();