from flask import Flask, render_template, url_for, jsonify, request
from redis import Redis, RedisError
import os
import socket
import twitter
import json
import hashlib

consumer_key = 'cPAxE5RSo0I3rvJh7qBxMj3rM'
consumer_secret = 'ZpajwZQM8IvIbIgFdVlSgnMth5RGkDI9wUGBRniLAx2sJ5Y9if'
access_token = '99014105-ISsaEEfJ5YDThQ2Q7BfviJupDFWrgZTAI2zN9VYT4'
access_secret = 'OVKfWd1EczmCPdRvptsEqhQcMCjzrqbSju9UaPu53tOHt'

map_api = 'AIzaSyC2cV9xKRu8z9cpn9byRcca_oRRHtxr3ok'

redis_url = os.getenv('REDISTOGO_URL', 'redis:X3ZX0OievYbgc2Xk3o4hhtsggHhygKUC@redis-18041.c16.us-east-1-3.ec2.cloud.redislabs.com:18041')

# redis = Redis(host="redis:X3ZX0OievYbgc2Xk3o4hhtsggHhygKUC@redis-18041.c16.us-east-1-3.ec2.cloud.redislabs.com:18041", db=0, socket_connect_timeout=2, socket_timeout=2)

# redis = Redis.from_url(redis_url)

redis = Redis(
    host='redis-18041.c16.us-east-1-3.ec2.cloud.redislabs.com',
    port=18041, 
    password='X3ZX0OievYbgc2Xk3o4hhtsggHhygKUC')


twt = twitter.Api(consumer_key=consumer_key,
                  consumer_secret=consumer_secret,
                  access_token_key=access_token,
                  access_token_secret=access_secret)

app = Flask(__name__)

@app.route("/")
def index():
	data = {}

	return render_template('base.html', data=data)

@app.route("/search")
def search():
	if( request.args ):

		radius = request.args["radius"]
		lat = request.args["lat"]
		lng = request.args["lng"]
		q = request.args["q"]
		data_from = "__default__"

		hash = hashlib.sha224(q).hexdigest()
		key = "search:" + hash

		if( redis.get(key) ):
			hastags = redis.get(key)
			data_from = "RedisCache"
		else:
			hastags = twt.GetSearch(raw_query="q="+q+"&geocode=" + lat + "," + lng + "," + radius + "km" +"&count=100")
			redis.set(key, json.dumps(hastags))
			redis.expire(key, 60)
			data_from = "TwitterAPI"

		search = {
			"data_from": data_from,
			"q": q,
			"radius": radius, 
			"lat": lat, 
			"lng": lng,
			"results": hastags
		}

		return json.dumps(search)

	return 'invalid request'

if __name__ == "__main__":
	app.run(host='0.0.0.0', port=80, debug=True)